import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs';

import { config } from './config';

import { Materia } from '../models/materia';
import { Tema } from '../models/tema';

@Injectable()
export class MateriasService {

  materias: Materia[] = [];

  constructor(private http: Http) {}

  getMaterias(){
    return this.http.get(config.server+"Materias/get");
  }

}

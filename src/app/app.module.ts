import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule } from '@angular/http';

import { RouterModule, Routes } from '@angular/router';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MateriasService } from './services/materias.service';
import { OtroComponent } from './otro/otro.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NarradorComponent } from './narrador/narrador.component';
import { EscritorComponent } from './escritor/escritor.component';
import { GeneradorComponent } from './generador/generador.component';

const rutasApp: Routes = [
  { 
    path: 'home', component: HomeComponent
  },
  { 
    path: 'otro', component: OtroComponent
  },
  {
    path: 'generador', component: GeneradorComponent
  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: NotFoundComponent }
];

export let menu = [];
rutasApp.forEach(function(element){
  if(element.path != "" && element.path != "**"){
    menu.push([element.path,element.path.replace(/\b\w/g, l => l.toUpperCase())]);
  }
});


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OtroComponent,
    NotFoundComponent,
    NarradorComponent,
    EscritorComponent,
    GeneradorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      rutasApp,
      { enableTracing: false } // <-- debugging purposes only
    ),
    HttpModule
  ],
  providers: [
    MateriasService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

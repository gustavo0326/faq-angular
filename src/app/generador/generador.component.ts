import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-generador',
  templateUrl: './generador.component.html',
  styleUrls: ['./generador.component.scss']
})
export class GeneradorComponent implements OnInit {

  img: any;

  constructor() { }

  ngOnInit() {
  }

  crearMateria(formulario: NgForm){
    console.log(formulario.value);
    let file: any = document.querySelector("#image");
    file = file.files[0];

    let blob = this.getFileBlob(file);
    blob.then(
      blob => this.img = blob
    );

  }

  /*
  *
  * Método para convertir una URL de un archivo en un
  * blob
  * @author: pabhoz
  *
  */
  getFileBlob(file) {
    
          var reader = new FileReader();
          return new Promise(function(resolve, reject){
    
            reader.onload = (function(theFile) {
              return function(e) {
                   resolve(e.target.result);
              };
            })(file);
    
            reader.readAsDataURL(file);
    
          });
     
      }

}

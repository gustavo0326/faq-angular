import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-narrador',
  templateUrl: './narrador.component.html',
  styleUrls: ['./narrador.component.scss']
})
export class NarradorComponent implements OnInit {

  @Input() texto: string;

  
  constructor() { }

  ngOnInit() {
  }

}

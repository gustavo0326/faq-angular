import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NarradorComponent } from './narrador.component';

describe('NarradorComponent', () => {
  let component: NarradorComponent;
  let fixture: ComponentFixture<NarradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NarradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NarradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
